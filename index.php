<?php include_once './src/header.php'  ?>

<main class="container">
	<h1 class="t-center">Lista de actividades</h1>
	<div class="list-container">
		<div class="sidebar">
			<ul>
				<li>
					<button href="#" class="btn" id="btn1">Actividad #8</button>
				</li>
				<li>
					<button href="#" class="btn">Actividad #9</button>
				</li>
				<li>
					<a href="/views/act10/index.php" class="btn">Actividad #10</a>
				</li>
				<li>
					<button href="#" class="btn">Actividad #11</button>
				</li>
				<li>
					<button href="#" class="btn">Actividad #12</button>
				</li>
			</ul>
		</div>
		<div class="lecture">
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque praesentium culpa ex illo iusto eum ratione deserunt laborum, aliquid ab quaerat ipsa doloribus quod architecto modi officia nemo amet dolorum.
				Beatae adipisci voluptate veniam accusamus amet perspiciatis laborum id pariatur porro recusandae at aliquam cum, asperiores fugiat quam tenetur voluptatibus facere iusto nulla itaque earum? Totam neque iure impedit minus!
				Qui libero odit tempora debitis eum blanditiis ut eligendi mollitia modi consequatur, veniam sequi voluptatum omnis ratione, perferendis maxime placeat. Perspiciatis dignissimos temporibus animi ratione corporis omnis saepe quibusdam aperiam.
				Praesentium dignissimos quae sequi optio quod nobis repellat pariatur similique temporibus saepe esse, minima assumenda numquam magnam quis nulla neque animi aliquid excepturi. Reiciendis ipsum facere natus mollitia dolore quis!
				Explicabo sapiente vero possimus mollitia adipisci, praesentium, ullam nostrum accusantium corrupti labore qui nihil? Corporis optio, nobis dicta magni iste nesciunt! Tempore quaerat deserunt sed cum rerum autem, ipsam non.</p>
		</div>
	</div>
</main>


<footer class="site-footer">
	<h3>This page was made by ArmandoAlmanza</h3>
	<p>Solo es para mostrar mis trabajos de la escuela</p>
</footer>

<script src="/public/js/bundle.min.js"></script>
</body>

</html>