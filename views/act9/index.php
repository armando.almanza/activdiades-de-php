<html>
    <head>
        <link rel="stylesheet" href="./app.css" />
        <title>¡Una página con estilo!</title>
    </head>
    <body>
        <!-- Site navigation menu -->
        <ul class="navbar">
            <li><a href="index.html">Inicio</a></li>
            <li><a href="musings.html">Animales marinos</a></li>
            <li><a href="town.html">Animales terrestres</a></li>
            <li><a href="links.html">Animales voladores</a></li>
        </ul>

        <!-- Main content -->
        <main>
            <h1>¡Esta es una página con mucho estilo!</h1>
            <h2>Bienvenidos a mi página de animales con estilo.</h2>
            <p>
                Aún no tiene imagenes pero sí tiene estilo. También tiene ligas,
                aúnque no lleven a ningún lado
            </p>
            <p>Pronto tendrá más contenido, ¡así que tengan paciencia!</p>
        </main>

        <footer>
            Hecha con amor el
            <?php  echo date("d/m/Y")  ?>
            con mucho amor.
        </footer>
    </body>
</html>
