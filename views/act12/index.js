let x = 0;

document.addEventListener("DOMContentLoaded", (event) => {
    let change = window.setInterval(changeColor, 5000);
});

const swapColor = (color) => {
    document.body.style.background = color;
    console.log(`Cambide de color al ${color}`);
};

const changeColor = () => {
    x++;

    switch (x) {
        case 1:
            swapColor("#F6FFA4");
            break;

        case 2:
            swapColor("#F190B7");
            break;

        case 3:
            swapColor("#FAD9E6");
            break;

        case 4:
            swapColor("#A2D5AB");
            break;

        case 5:
            swapColor("#FDF6EC");
            break;
    }
};
