<?php

$db = new mysqli('localhost', 'root', 'root', 'act7');
if (!$db) {
	echo "Error, canot connected to the database";
	echo "errno de depuración: " . mysqli_connect_errno();
	echo "error de depuración: " . mysqli_connect_error();
	exit;
}

$all = "SELECT * FROM users";

if ($result = $db->query($all)) {
	while ($row = $result->fetch_assoc()) {
		$field1name = $row["id"];
		$field2name = $row["user_name"];
		$field3name = $row["gender"];
		$field4name = $row["favorite_color"];
		$field5name = $row["favorite_food"];

		echo ' 
		<div class="data">
			<h2 class="center subtitle">Data of ' . $field2name . '</h2>
			<ul> 
				  <li>' . 'ID: ' . $field1name . '</li> 
				  <li>' . 'Name: ' . $field2name . '</li> 
				  <li>' . 'Gender: ' . $field3name . '</li> 
				  <li>' . 'Favorite Color: ' . $field4name . '</li> 
				  <li>' . 'Favorite Food: ' . $field5name . '</li> 
			  </ul>
		</div>';
	}
	/* free result set */
	$result->free();
}
?>