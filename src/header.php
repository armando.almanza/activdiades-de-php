<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<link rel="icon" type="image/svg+xml" href="/src/favicon.png" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="../public/css/app.css">
	<title>Actividad</title>
</head>

<body>
	<!-- <div class="dnone">
		<h1 class="center">Registros</h1>
		<div>
			<h3 class="center">Filters</h3>
			<div class="btns">
				<a href="../index.php" class="btn">All the data</a>
				<a href="/views/act7/byNames.php" class="btn">By names</a>
				<a href="/views/act7/byGender.php" class="btn">By gender</a>
				<a href="/views/act7/byColor.php" class="btn">By colors</a>
				<a href="/views/act7/byFood.php" class="btn">By food</a>
			</div>
		</div> -->

	<header class="site-header">
		<div class="container header-container">
			<h1 class="logo">Armando<span>Almanza</span></h1>
			<nav class="navbar">
				<a href="https://github.com/ArmandoAlmanza" target="_blank">Github</a>
				<a href="https://gitlab.com/armando.almanza/activdiades-de-php" target="_blank">GitLab</a>
				<a href="https://drive.google.com/drive/folders/13jzTtsctLUzrDaUV4VFdy0gdA6W-8_XL?usp=sharing" target="_blank">Google Drive</a>
			</nav>
		</div>

		<h1 class="texto">Pagina para mostar mis trabajos de certificado</h1>

	</header>